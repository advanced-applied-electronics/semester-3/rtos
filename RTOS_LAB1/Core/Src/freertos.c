/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId blinkTaskHandle;
osThreadId ButtonTaskHandle;
osThreadId ResetPeriodTaskHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void blinkRoutine(void const * argument);
void buttonRoutine(void const * argument);
void ResetPeriodRoutine(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* Hook prototypes */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	void* ptr_BlinkLED_Period = (void*)&BlinkLED_Period;

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of blinkTask */
  osThreadDef(blinkTask, blinkRoutine, osPriorityNormal, 0, 128);
  blinkTaskHandle = osThreadCreate(osThread(blinkTask), (void*) ptr_BlinkLED_Period);

  /* definition and creation of ButtonTask */
  osThreadDef(ButtonTask, buttonRoutine, osPriorityNormal, 0, 128);
  ButtonTaskHandle = osThreadCreate(osThread(ButtonTask), (void*) ptr_BlinkLED_Period);

  /* definition and creation of ResetPeriodTask */
  osThreadDef(ResetPeriodTask, ResetPeriodRoutine, osPriorityNormal, 0, 128);
  ResetPeriodTaskHandle = osThreadCreate(osThread(ResetPeriodTask), (void*) ptr_BlinkLED_Period);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_blinkRoutine */
/**
* @brief Function implementing the blikTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_blinkRoutine */
void blinkRoutine(void const * argument)
{
  /* USER CODE BEGIN blinkRoutine */
  /* Infinite loop */
	static uint32_t half_period_ms;
	static uint32_t osPreviousWakeTime;
	osPreviousWakeTime= osKernelSysTick();
	for (;;) {
		half_period_ms = *((uint32_t*) argument);
		half_period_ms /= 2; // When using toggle we have to use half of period because there are two toggles per period.

		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		HAL_GPIO_TogglePin(SALEAE_GPIO_Port, SALEAE_Pin);

		/* We are using delay until because we want to get exact timing that includes code execution.
		 * It shouldn't matter in such simple case, but it is worth to remember about. */
		osDelayUntil(&osPreviousWakeTime, half_period_ms);
	}
  /* USER CODE END blinkRoutine */
}

/* USER CODE BEGIN Header_buttonRoutine */
/**
* @brief Function implementing the ButtonTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_buttonRoutine */
void buttonRoutine(void const * argument)
{
  /* USER CODE BEGIN buttonRoutine */
  /* Infinite loop */
	static uint32_t period_ms;
	for (;;) {
		if (ButtonPressed == true) {
			/* Read period */
			period_ms = *((uint32_t*)argument);

			/* Divide period by 2 (if possible) */
			if (period_ms>1)
				period_ms = period_ms/2;
			else
				period_ms = 1;

			/* "Return" new period value */
			*((uint32_t*)argument) = period_ms;

			ButtonPressed = false;
		}
		osDelay(CRUDE_DEBOUNCE_PERIOD);
	}
  /* USER CODE END buttonRoutine */
}

/* USER CODE BEGIN Header_ResetPeriodRoutine */
/**
* @brief Function implementing the ResetPeriodTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ResetPeriodRoutine */
void ResetPeriodRoutine(void const * argument)
{
  /* USER CODE BEGIN ResetPeriodRoutine */
  /* Infinite loop */
	for (;;) {
		if (ResetGPIOShortedToGND == true) {
			*((uint32_t*) argument) = BLINK_LED_DEFAULT_PERIOD;

			ResetGPIOShortedToGND = false;
		}
		osDelay(CRUDE_DEBOUNCE_PERIOD);
	}
  /* USER CODE END ResetPeriodRoutine */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */
