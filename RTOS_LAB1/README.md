# Real Time Operating Systems Laboratory - LAB 1

**Table of contents:**

[[_TOC_]]

## Authors

[**Krystian Matusiak**](https://gitlab.com/krystian_matusiak "@krystian_matusiak") 249460

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel "@piter_t_ziel") 237457

## Laboratory 1 tasks

### Rules

Task should be done in such way to meet all following requirements:

- all exercises should be performed on ```STM32F446``` boards, provided by theTeacher,

- during implementation put some meaningful comments,

- during implementation use meaningful names for structures, variables etc,

- prepare task solution documentation in the form of report - ```*.md``` (markdown file) or ```*.pdf```

### Task to do

- (10 pts) Get know ```STM32F446``` board, list LEDs and buttons and their physical connection.

- (20 pts) Prepare a simple FreeRTOS application, capable of blinking led light with frequency equal to 10Hz. Focus on structure of generated code, mind appropriate places for putting custom code.

- (60 pts) Prepare an application that has 2 tasks running in parallel:

    - first task for blinking a LED with frequency defined as modifiable parameter with some initial value 10,
    
    - second task running periodically every 1s for checking whether button has ever been pressed (no matter what amount of times). When press was detected,

    - modifiable parameter shall be increased with additional chosen value.

- (20 pts*) add to application 3rd task, associated with chosen GPIO pin. When given GPIO pin will be shortcut with GND, then the setting of modifiable parameter shall return to initial value.

### Realization details

- when preparing application in ```FreeRTOS```, use ```STM32CubeMX```. Focus on proper selection of peripherals, use ```CMSIS V1 FreeRTOS``` mode,

- first task shall continuously working with delay set for toggling the LED.

- pressing the button shall modify in ISR state of global variable.
