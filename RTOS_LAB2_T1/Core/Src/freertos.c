/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "adc.h"
#include "stm32f4xx_ll_adc.h"
#include <stdio.h>
#include "arm_math.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticTask_t osStaticThreadDef_t;
/* USER CODE BEGIN PTD */

#define WindowLength 	(20)

#define FFT_SAMPLES		(512)
#define FFT_SIZE 		(FFT_SAMPLES / 2)

typedef struct Filter_t{
	uint32_t History[WindowLength]; /*Array to store values of filter window*/
	uint32_t Sum;					/* Sum of filter window's elements*/
	uint32_t WindowPointer; 		/* Pointer to the first element of window*/
}Filter_t;

typedef struct raw_measurement_t{
	uint16_t voltage;
	uint16_t temperature;
}raw_measurement_t;

typedef struct results_t{
	float32_t frequency;
	uint32_t temperature;
}results_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define IGNORE_MSG_PRIO (0)

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
uint32_t defaultTaskBuffer[ 1024 ];
osStaticThreadDef_t defaultTaskControlBlock;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .cb_mem = &defaultTaskControlBlock,
  .cb_size = sizeof(defaultTaskControlBlock),
  .stack_mem = &defaultTaskBuffer[0],
  .stack_size = sizeof(defaultTaskBuffer),
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for Task_A */
osThreadId_t Task_AHandle;
uint32_t Task_ABuffer[ 1024 ];
osStaticThreadDef_t Task_AControlBlock;
const osThreadAttr_t Task_A_attributes = {
  .name = "Task_A",
  .cb_mem = &Task_AControlBlock,
  .cb_size = sizeof(Task_AControlBlock),
  .stack_mem = &Task_ABuffer[0],
  .stack_size = sizeof(Task_ABuffer),
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for PrintTask */
osThreadId_t PrintTaskHandle;
uint32_t PrintTaskBuffer[ 1024 ];
osStaticThreadDef_t PrintTaskControlBlock;
const osThreadAttr_t PrintTask_attributes = {
  .name = "PrintTask",
  .cb_mem = &PrintTaskControlBlock,
  .cb_size = sizeof(PrintTaskControlBlock),
  .stack_mem = &PrintTaskBuffer[0],
  .stack_size = sizeof(PrintTaskBuffer),
  .priority = (osPriority_t) osPriorityAboveNormal,
};
/* Definitions for measurementQueue */
osMessageQueueId_t measurementQueueHandle;
const osMessageQueueAttr_t measurementQueue_attributes = {
  .name = "measurementQueue"
};
/* Definitions for printingQueue */
osMessageQueueId_t printingQueueHandle;
const osMessageQueueAttr_t printingQueue_attributes = {
  .name = "printingQueue"
};
/* Definitions for Timer1 */
osTimerId_t Timer1Handle;
const osTimerAttr_t Timer1_attributes = {
  .name = "Timer1"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

void Moving_Average_Init(Filter_t* filter_struct);
uint32_t Moving_Average_Compute(uint32_t raw_data, Filter_t* filter_struct);

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void Task_A_Function(void *argument);
void printTask(void *argument);
void Timer1_Callback(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName);
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
	return HAL_GetTick();
}
/* USER CODE END 1 */

/* USER CODE BEGIN 4 */
void vApplicationStackOverflowHook(xTaskHandle xTask, signed char *pcTaskName)
{
	UNUSED(xTask);
	printf("Stack overflow for task: %s", pcTaskName);

   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of Timer1 */
  Timer1Handle = osTimerNew(Timer1_Callback, osTimerPeriodic, NULL, &Timer1_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of measurementQueue */
  measurementQueueHandle = osMessageQueueNew (16, sizeof(raw_measurement_t), &measurementQueue_attributes);

  /* creation of printingQueue */
  printingQueueHandle = osMessageQueueNew (40, sizeof(results_t), &printingQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Task_A */
  Task_AHandle = osThreadNew(Task_A_Function, NULL, &Task_A_attributes);

  /* creation of PrintTask */
  PrintTaskHandle = osThreadNew(printTask, NULL, &PrintTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
	UNUSED(argument);
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_Task_A_Function */
/**
* @brief Function implementing the Task_A thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task_A_Function */
void Task_A_Function(void *argument)
{
  /* USER CODE BEGIN Task_A_Function */
	UNUSED(argument);
	static raw_measurement_t rawPacket;
	static results_t readyPacket;

	static uint32_t temperature;

	/* Init walking average */
	Filter_t avgFilter;
	Moving_Average_Init(&avgFilter);

	/* Init FFT */
	arm_cfft_radix4_instance_f32 S; /* ARM CFFT module */
	float32_t maxValue; /* Max FFT value is stored here */
	uint32_t maxIndex; /* Index in Output array where max value is */
	uint16_t i = 0;
	float32_t Input[FFT_SAMPLES];
	float32_t Output[FFT_SIZE];

	/* Start timer triggering ADC */
	static const uint32_t timerPeriod_ms = 5;
	if (osTimerStart(Timer1Handle, (timerPeriod_ms * portTICK_PERIOD_MS))
			!= osOK) {
		for (;;)
			; /* failure!?! */
	}

	/* Infinite loop */
	for (;;) {
		/* Check if queue is empty */
		if (osMessageQueueGetCount(measurementQueueHandle) != 0) {
			/* Extract one packet from queue */
			osMessageQueueGet(measurementQueueHandle, (void*) (&rawPacket), IGNORE_MSG_PRIO,
					0);

			/* Calculate temperature */
			temperature = __LL_ADC_CALC_TEMPERATURE(3300, rawPacket.temperature,
					LL_ADC_RESOLUTION_12B);

			/* Insert measured temperature into walking average buffer.	 */
			temperature = Moving_Average_Compute((uint32_t) temperature,
					&avgFilter);

			/* Store value from ADC for FFT*/
			Input[i] = (float32_t) (rawPacket.voltage) / 4095 * 3.3f - 1.75f; // Real part
			Input[i + 1] = (float32_t) (0);					// Imaginary part

			i += 2;

			if (i >= FFT_SAMPLES) {
				/* Initialize the CFFT/CIFFT module, intFlag = 0, doBitReverse = 1 */
				arm_cfft_radix4_init_f32(&S, FFT_SIZE, 0, 1);

				/* Process the data through the CFFT/CIFFT module */
				arm_cfft_radix4_f32(&S, Input);

				/* Process the data through the Complex Magniture Module for calculating the magnitude at each bin */
				arm_cmplx_mag_f32(Input, Output, FFT_SIZE);

				/* Calculates maxValue and return corresponding value */
				arm_max_f32(Output + 1, FFT_SIZE / 2 - 1, &maxValue, &maxIndex);

				/* Print data */
				int freq;
				for (i = 0; i < FFT_SIZE / 2; i++) {
					freq = (int32_t)((float)(i) / (FFT_SIZE / 2)*100);
					printf("%d Hz: %f\n\r", freq, Output[(uint16_t)i]);
				}
				/* Queue measured values for printing */
				readyPacket.temperature = temperature;
				readyPacket.frequency = ((float) (maxIndex + 1) / (FFT_SIZE / 2)
						* 100);
				osMessageQueuePut(printingQueueHandle, (void*) (&readyPacket),
						IGNORE_MSG_PRIO, 0);
				i = 0;
			}
		}

		osDelay(1);
	}
  /* USER CODE END Task_A_Function */
}

/* USER CODE BEGIN Header_printTask */
/**
* @brief Function implementing the PrintTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_printTask */
void printTask(void *argument)
{
  /* USER CODE BEGIN printTask */
	UNUSED(argument);
  /* Infinite loop */
	static results_t package;
  for(;;)
  {
	  /* Check if there are any measurements ready for printing */
	  if (osMessageQueueGetCount(printingQueueHandle) != 0)
	  {
		  osMessageQueueGet(printingQueueHandle, (void*)&package, IGNORE_MSG_PRIO, 0);

		  /* Print to serial port using printf redirected to UART 2 */
		  printf("Avg temp: %d *C. \t Measured frequency: %.3f Hz.\n\r", package.temperature, package.frequency);
	  }
	  osDelay(1);
  }
  /* USER CODE END printTask */
}

/* Timer1_Callback function */
void Timer1_Callback(void *argument)
{
  /* USER CODE BEGIN Timer1_Callback */
	UNUSED(argument);

	HAL_GPIO_WritePin(ADC_TIMING_GPIO_Port, ADC_TIMING_Pin, GPIO_PIN_SET);

	/* Get measurements in polling mode */
	raw_measurement_t tmp_mes;

	/* Measure temperature */
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
	tmp_mes.temperature = (uint16_t)HAL_ADC_GetValue(&hadc1);

	/* Measure voltage */
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY);
	tmp_mes.voltage = (uint16_t)HAL_ADC_GetValue(&hadc1);

	/* Copy measured values to queue */
	osMessageQueuePut(measurementQueueHandle, (void *)(&tmp_mes), IGNORE_MSG_PRIO, 0);
	HAL_GPIO_WritePin(ADC_TIMING_GPIO_Port, ADC_TIMING_Pin, GPIO_PIN_RESET);

  /* USER CODE END Timer1_Callback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */


/**
  * @brief  This function initializes filter's data structure.
	* @param  filter_struct : Data structure
  * @retval None.
  */
void Moving_Average_Init(Filter_t* filter_struct)
{
	filter_struct->Sum = 0;
	filter_struct->WindowPointer = 0;

	for(uint32_t i=0; i<WindowLength; i++)
	{
		filter_struct->History[i] = 0;
	}
}

/**
  * @brief  This function filters data with moving average filter.
	* @param  raw_data : input raw sensor data.
	* @param  filter_struct : Data structure
  * @retval Filtered value.
  */
uint32_t Moving_Average_Compute(uint32_t raw_data, Filter_t* filter_struct)
{
	filter_struct->Sum += raw_data;
	filter_struct->Sum -= filter_struct->History[filter_struct->WindowPointer];
	filter_struct->History[filter_struct->WindowPointer] = raw_data;
	if(filter_struct->WindowPointer < WindowLength - 1)
	{
		filter_struct->WindowPointer += 1;
	}
	else
	{
		filter_struct->WindowPointer = 0;
	}
	return filter_struct->Sum/WindowLength;
}

/* USER CODE END Application */

