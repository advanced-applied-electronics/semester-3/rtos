/*
 * utils.h
 *
 *  Created on: May 24, 2023
 *      Author: Piotr Zieliński
 */

#ifndef LIB_UTILS_UTILS_H_
#define LIB_UTILS_UTILS_H_

#include "stdint.h"


float mean_f32(float* data, uint32_t len);
float mean_square_f32(float* data, uint32_t len);
void generate_randoms_f32(float* data_dst, uint32_t len, float lower, float upper);


#endif /* LIB_UTILS_UTILS_H_ */
