/*
 * utils.c
 *
 *  Created on: May 24, 2023
 *      Author: Piotr Zieliński
 */

#include "utils.h"
#include "math.h"
#include "main.h"

/**
 * @brief Calculate mean value of given data
 * @param data Mean value will be calculated for this data
 * @param len Length of data
 * @return Mean value of input data
 */
float mean_f32(float* data, uint32_t len)
{
	if (data == NULL)
		while (1);

	if (len == 0)
		while (1);


	float mean = 0;

	for (uint32_t i=0; i < len; i++)
	{
		mean += data[i];
	}
	mean /= len;

	return mean;
}

/**
 * @brief Calculate mean square of given data
 * @param data	Mean square value will be calculated for this data
 * @param len	Length of data
 * @return	mean square value
 */
float mean_square_f32(float* data, uint32_t len)
{
	if (data == NULL)
		while (1);

	if (len == 0)
		while (1);
	float mean_square = 0;

	for (uint32_t i=0; i<len; i++)
	{
		mean_square += powf(data[i], 2);
	}

	mean_square /= len;

	return mean_square;
}

/**
 *
 * @param data_dst Random numbers will be generated into this array
 * @param len	Length of data
 * @param lower	lower limit of random numbers
 * @param upper	upper limit of random numbers
 */
void generate_randoms_f32(float* data_dst, uint32_t len, float lower, float upper)
{
	if (data_dst == NULL)
		while(1);
	if (len == 0)
		return;

	float range = upper - lower;

    for (uint32_t i = 0; i < len; i++) {
    	float scale = rand() / (float) RAND_MAX; /* [0, 1.0] */
    	data_dst[i] = lower + scale * range;      /* [min, max] */
    }
}
