# Real Time Operating Systems Laboratory

The aim of this classes was to familiarize with FreeRTOS applied on microcontroller STM32F466RE.

**Table of contents:**

[[_TOC_]]

## Authors

[**Krystian Matusiak**](https://gitlab.com/krystian_matusiak "@krystian_matusiak") 249460

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel "@piter_t_ziel") 237457

## Laboratory 1

First task was a warm up - there was a need to create 3 tasks:

* LED that blinks with particular frequency

* button responsible for doubling the blinking frequency

* button responsible for reseting blinking frequency to initial.

## Laboratory 2

### Rules

The source code needs to be pushed to a git server.
Please, prepare task solution documentation in the form of a report: i.e. *.md (markdown file) or *.pdf, and upload it below.

### Scenario

It is necessary to periodically (at fixed intervals) take a sample from the ADC from the temperature channel of the microcontroller and from any additional channel connected to the selected pin. Then the samples (as a structure) should be sent to the queue of task_A. Task_A waits in the queue for data from the ADC and calculates two values: the average temperature of the microcontroller and the basic frequency of the analog signal on the selected pin.

### ToDo

* (25 pts) - to collect data from ADC use a software timer provided by FrreRTOS and a timer callback function.

* (25 pts) - to collect data from ADC use a hardware timer that drives ADC, next IRQ service takes data and sent it to the task_A.

* (25 pts) - configure the DMA channel and connect it with ADC to the automatic transmission of data between ADC and memory buffer.

* (25 pts*) - write an additional task that communicates with the PC (through USART/USB) and send this way the results of the calculation of task_A to the PC.

### Details

* The report should provide the results of a measurement of microcontroller utilization, and also measurements of the stability of a period of data collections

* ad point 3. - the DMA has to work in a circular configuration, a buffer has to be divided in half and IRQ should rise when the buffer is half full and when it is full - so-called "ping-pong" configuration

* an analog signal connected to the chosen pin can be taken from a generator or from the "finger", thus expected frequency is below 100 Hz.

* the name of task_A is arbitrary

* an analog pin and consequently ADC channel is arbitrary, temperature channel has the number IN16

Hint - example code taking values from two configured channels:

```C
    // Get ADCs here
    HAL_ADC_Start(&hadc);
     
    HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
    ADCVolt = HAL_ADC_GetValue(&hadc);
     
    HAL_ADC_PollForConversion(&hadc, HAL_MAX_DELAY);
    ADCTemp = HAL_ADC_GetValue(&hadc);
     
    HAL_ADC_Stop(&hadc);
```

## Laboratory 3

### Scenario for lab 3

A UART/USART port prints the data structure once a second. The structure is complicated - i.e contains many fields filled by different tasks. Let's say that there exist three tasks that publish the results of their calculations in the structure. The task which implements communication by UART/USART calculates the mean value of the results published by tasks.
Let each task generate a 1000 random value in the range of ±1.0. Next a task should calculate mean and mean square values. Both values have to be published in the global structure. Thus UART task can finally calculate the mean of all means and the mean of all means square.

### To Do for lab 3

* stage_1 (40 pts.) - Solve the concurrency problem using semaphore

* stage_2 (60 pts.) - Solve the concurrency problem using mutex

### Rules for lab 3

* don't use queues

* each task should have a different priority, please, experiment with the different priorities of monitor and math threads

* the monitor thread has to wait till all math threads finish its jobs, thus, a solution to a synchronization problem should be proposed. The possible solutions are:

    * to use second semaphore to informthe monitor about finishing a math job,

    * to use a direct to task notyfication mechanizm implemented in FreeRTOS (look at API documentation on the web page)

* UART communication protocol is intentionally slowed down (i.e. osDelay(10) after each character) and additionally, the monitor thread blocks access to the data structure (i.e. taking semaphore/mutex) till the end of print. osDelay() should be added to __io_putchar() implementation

* the use of mutex instead of semaphore will protect you against so called "priority inversion" problem, so, one would expect to observe diffrent behavior of whole system comparing semaphore & mutex solutions.

### Report

The report should contain answers to the questions and show how it was solved:

* What happens when tasks have a priority higher than UART?

* What happens when tasks have a priority lower than UART?

* Have you noticed the difference between semaphore and mutex? If so, please, provide a figures form the LogicAnalyzer.

## Laboratory 4 - Stream Buffers experiments

### Scenario for lab 4

* a thread called monitor (through UART or display)
    * should be implemented using StreamBuffer mechanism
        * trigger level = 1
    * other threads have to use xStreamBufferSend()
    * monitor uses xStreamBufferReceive() with some small timeout 
* ADC should constantly work
    * only one analog channel has to be sampled
    * any analog channel can be chosen 
    * inside the ISR caused by EOF:
        * a sample has to be taken, and if the value of the sample is higher than some threshold then the message (i.e. character string) should be sent to the StreamBuffer using xStreamBufferSendFromISR()
        * one can set up a new conversion at the end of the ISR callback, or any hardware TIM can be utilized
* There should exist two other threads/tasks being reading a value from some sensors placed on the same I2C/SPI bus. Therefore, both threads have to compete for access to a bus. So, the bus driver must implement mutex/semaphore protection.
    * sensors can be chosen among given by a teacher
    * in case of a lack of enough sensors, both threads can take data from the same sensor (i.e. the only one)

### To do for lab 4

(100 pts.) - Implementation of all necessary threads and ADC interrupt callback
