/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "gpio.h"
#include "../lib/utils/utils.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

#define NUMBER_OF_NUMBERS 	(1000)
#define TASK_DELAY 			(1) //ms

#define TASK1_RDY_BIT_POS	(0)
#define TASK2_RDY_BIT_POS 	(1)
#define TASK3_RDY_BIT_POS	(2)

#define TASK1_RDY_NOTIF_FLAG (1 << TASK1_RDY_BIT_POS)
#define TASK2_RDY_NOTIF_FLAG (1 << TASK2_RDY_BIT_POS)
#define TASK3_RDY_NOTIF_FLAG (1 << TASK3_RDY_BIT_POS)

#define ALL_TASKS_RDY (TASK1_RDY_NOTIF_FLAG | TASK2_RDY_NOTIF_FLAG | TASK3_RDY_NOTIF_FLAG)

#define TASK_CAN_CALCULATE_NOTIF_FLAG (0x25)



#define DO_NOT_WAIT (0)

#define SEMAF
//#define MUTEX

typedef struct MeanStorage_t
{
	float meanTask1;
	float meanTask2;
	float meanTask3;

	float meanSquareTask1;
	float meanSquareTask2;
	float meanSquareTask3;
}MeanStorage_t;

MeanStorage_t meanStorage;


/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for Task1 */
osThreadId_t Task1Handle;
const osThreadAttr_t Task1_attributes = {
  .name = "Task1",
  .stack_size = 1250 * 4,
  .priority = (osPriority_t) osPriorityLow4,
};
/* Definitions for Task2 */
osThreadId_t Task2Handle;
const osThreadAttr_t Task2_attributes = {
  .name = "Task2",
  .stack_size = 1250 * 4,
  .priority = (osPriority_t) osPriorityLow3,
};
/* Definitions for Task3 */
osThreadId_t Task3Handle;
const osThreadAttr_t Task3_attributes = {
  .name = "Task3",
  .stack_size = 1250 * 4,
  .priority = (osPriority_t) osPriorityLow2,
};
/* Definitions for Printer */
osThreadId_t PrinterHandle;
const osThreadAttr_t Printer_attributes = {
  .name = "Printer",
  .stack_size = 300 * 4,
  .priority = (osPriority_t) osPriorityLow1,
};
/* Definitions for structAccessMutex */
osMutexId_t structAccessMutexHandle;
const osMutexAttr_t structAccessMutex_attributes = {
  .name = "structAccessMutex"
};
/* Definitions for structAccessSema */
osSemaphoreId_t structAccessSemaHandle;
const osSemaphoreAttr_t structAccessSema_attributes = {
  .name = "structAccessSema"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void Task1Func(void *argument);
void Task2Func(void *argument);
void Task3Func(void *argument);
void PrinterFunc(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void vApplicationMallocFailedHook(void);

/* USER CODE BEGIN 5 */
void vApplicationMallocFailedHook(void)
{
   /* vApplicationMallocFailedHook() will only be called if
   configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h. It is a hook
   function that will get called if a call to pvPortMalloc() fails.
   pvPortMalloc() is called internally by the kernel whenever a task, queue,
   timer or semaphore is created. It is also called by various parts of the
   demo application. If heap_1.c or heap_2.c are used, then the size of the
   heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
   FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
   to query the size of free heap space that remains (although it does not
   provide information on how the remaining heap might be fragmented). */
}
/* USER CODE END 5 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

	meanStorage.meanTask1 = 0;
	meanStorage.meanTask2 = 0;
	meanStorage.meanTask3 = 0;

	meanStorage.meanSquareTask1 = 0;
	meanStorage.meanSquareTask2 = 0;
	meanStorage.meanSquareTask3 = 0;

  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of structAccessMutex */
  structAccessMutexHandle = osMutexNew(&structAccessMutex_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of structAccessSema */
  structAccessSemaHandle = osSemaphoreNew(1, 1, &structAccessSema_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Task1 */
  Task1Handle = osThreadNew(Task1Func, NULL, &Task1_attributes);

  /* creation of Task2 */
  Task2Handle = osThreadNew(Task2Func, NULL, &Task2_attributes);

  /* creation of Task3 */
  Task3Handle = osThreadNew(Task3Func, NULL, &Task3_attributes);

  /* creation of Printer */
  PrinterHandle = osThreadNew(PrinterFunc, NULL, &Printer_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_Task1Func */
/**
* @brief Function implementing the Task1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task1Func */
void Task1Func(void *argument)
{
  /* USER CODE BEGIN Task1Func */
	float data[NUMBER_OF_NUMBERS];
  /* Infinite loop */
  for(;;)
  {
	  /* Wait for notification from printer task */
	  osStatus_t result =  osThreadFlagsWait(TASK_CAN_CALCULATE_NOTIF_FLAG, osFlagsWaitAll, osWaitForever);

	  /* Enter critical section */
#ifdef SEMAF
	  osSemaphoreAcquire(structAccessSemaHandle, osWaitForever);
#else
	  osMutexAcquire(structAccessMutexHandle, osWaitForever);
#endif
	  HAL_GPIO_TogglePin(Task1GPIO_GPIO_Port, Task1GPIO_Pin);

	  /* Generate random data */
	  generate_randoms_f32(data, NUMBER_OF_NUMBERS, -1.0f, 1.0f);

	  /* Calculate mean value of data */
	  meanStorage.meanTask1 = mean_f32(data, NUMBER_OF_NUMBERS);

	  /* Calculate mean square value of data */
	  meanStorage.meanSquareTask1 = mean_square_f32(data, NUMBER_OF_NUMBERS);

	  HAL_GPIO_TogglePin(Task1GPIO_GPIO_Port, Task1GPIO_Pin);

	  /* Leave critical section */
#ifdef SEMAF
	  osSemaphoreRelease(structAccessSemaHandle);
#else
	  osMutexRelease(structAccessMutexHandle);
#endif

	  /* Let printer task know that this task finished calculations */
	  osThreadFlagsSet(PrinterHandle, TASK1_RDY_NOTIF_FLAG);

	  osDelay(TASK_DELAY);
  }
  /* USER CODE END Task1Func */
}

/* USER CODE BEGIN Header_Task2Func */
/**
* @brief Function implementing the Task2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task2Func */
void Task2Func(void *argument)
{
  /* USER CODE BEGIN Task2Func */
	float data[NUMBER_OF_NUMBERS];
  /* Infinite loop */
  for(;;)
  {
	  /* Wait for notification from printer task */
	  osStatus_t result =  osThreadFlagsWait(TASK_CAN_CALCULATE_NOTIF_FLAG, osFlagsWaitAll, osWaitForever);

	  /* Enter critical section */
#ifdef SEMAF
	  osSemaphoreAcquire(structAccessSemaHandle, osWaitForever);
#else
	  osMutexAcquire(structAccessMutexHandle, osWaitForever);
#endif
	  HAL_GPIO_TogglePin(Task2GPIO_GPIO_Port, Task2GPIO_Pin);

	  /* Generate random data */
	  generate_randoms_f32(data, NUMBER_OF_NUMBERS, -1.0f, 1.0f);

	  /* Calculate mean value of data */
	  meanStorage.meanTask2 = mean_f32(data, NUMBER_OF_NUMBERS);

	  /* Calculate mean square value of data */
	  meanStorage.meanSquareTask2 = mean_square_f32(data, NUMBER_OF_NUMBERS);

	  HAL_GPIO_TogglePin(Task2GPIO_GPIO_Port, Task2GPIO_Pin);

	  /* Leave critical section */
#ifdef SEMAF
	  osSemaphoreRelease(structAccessSemaHandle);
#else
	  osMutexRelease(structAccessMutexHandle);
#endif

	  /* Let printer task know that this task finished calculations */
	  osThreadFlagsSet(PrinterHandle, TASK2_RDY_NOTIF_FLAG);

	  osDelay(TASK_DELAY);
  }
  /* USER CODE END Task2Func */
}

/* USER CODE BEGIN Header_Task3Func */
/**
* @brief Function implementing the Task3 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Task3Func */
void Task3Func(void *argument)
{
  /* USER CODE BEGIN Task3Func */
	float data[NUMBER_OF_NUMBERS];
  /* Infinite loop */
  for(;;)
  {
	  /* Wait for notification from printer task */
	  osStatus_t result =  osThreadFlagsWait(TASK_CAN_CALCULATE_NOTIF_FLAG, osFlagsWaitAll, osWaitForever);

	  /* Enter critical section */
#ifdef SEMAF
	  osSemaphoreAcquire(structAccessSemaHandle, osWaitForever);
#else
	  osMutexAcquire(structAccessMutexHandle, osWaitForever);
#endif
	  HAL_GPIO_TogglePin(Task3GPIO_GPIO_Port, Task3GPIO_Pin);

	  /* Generate random data */
	  generate_randoms_f32(data, NUMBER_OF_NUMBERS, -1.0f, 1.0f);

	  /* Calculate mean value of data */
	  meanStorage.meanTask3 = mean_f32(data, NUMBER_OF_NUMBERS);

	  /* Calculate mean square value of data */
	  meanStorage.meanSquareTask3 = mean_square_f32(data, NUMBER_OF_NUMBERS);

	  HAL_GPIO_TogglePin(Task3GPIO_GPIO_Port, Task3GPIO_Pin);

	  /* Leave critical section */
#ifdef SEMAF
	  osSemaphoreRelease(structAccessSemaHandle);
#else
	  osMutexRelease(structAccessMutexHandle);
#endif
	  /* Let printer task know that this task finished calculations */
	  osThreadFlagsSet(PrinterHandle, TASK3_RDY_NOTIF_FLAG);

	  osDelay(TASK_DELAY);
  }
  /* USER CODE END Task3Func */
}

/* USER CODE BEGIN Header_PrinterFunc */
/**
* @brief Function implementing the Printer thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_PrinterFunc */
void PrinterFunc(void *argument)
{
  /* USER CODE BEGIN PrinterFunc */

	/* Let tasks know that they can start calculations */
	osThreadFlagsSet(Task1Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);
	osThreadFlagsSet(Task2Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);
	osThreadFlagsSet(Task3Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);

  /* Infinite loop */
  for(;;)
  {

	  /* Wait for notification from printer task */
	  osThreadFlagsWait(ALL_TASKS_RDY, osFlagsWaitAll, osWaitForever);

	  /* Enter critical section */
#ifdef SEMAF
	  osSemaphoreAcquire(structAccessSemaHandle, osWaitForever);
#else
	  osMutexAcquire(structAccessMutexHandle, osWaitForever);
#endif
	  HAL_GPIO_TogglePin(PrinterGPIO_GPIO_Port, PrinterGPIO_Pin);

	  float meanOfMeans = 0;
	  meanOfMeans += meanStorage.meanTask1;
	  meanOfMeans += meanStorage.meanTask2;
	  meanOfMeans += meanStorage.meanTask3;
	  meanOfMeans /= 3;

	  float meanOfMeanSquares = 0;
	  meanOfMeanSquares += meanStorage.meanSquareTask1;
	  meanOfMeanSquares += meanStorage.meanSquareTask2;
	  meanOfMeanSquares += meanStorage.meanSquareTask3;
	  meanOfMeanSquares /= 3;

	  printf("Mean of means = %f\r\n", meanOfMeans);
	  printf("Mean of mean squares = %f\r\n", meanOfMeanSquares);
	  osDelay(10);


	  HAL_GPIO_TogglePin(PrinterGPIO_GPIO_Port, PrinterGPIO_Pin);

	  /* Leave critical section */
#ifdef SEMAF
	  osSemaphoreRelease(structAccessSemaHandle);
#else
	  osMutexRelease(structAccessMutexHandle);
#endif

	  /* Let tasks know that they can start calculations */
	  osThreadFlagsSet(Task1Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);
	  osThreadFlagsSet(Task2Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);
	  osThreadFlagsSet(Task3Handle, TASK_CAN_CALCULATE_NOTIF_FLAG);

	  osDelay(TASK_DELAY);
  }
  /* USER CODE END PrinterFunc */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

