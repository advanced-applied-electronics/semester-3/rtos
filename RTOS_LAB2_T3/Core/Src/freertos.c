/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "adc.h"
#include "tim.h"
#include "arm_math.h"
#include "gpio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

#define WindowLength 	(20)

#define FFT_SAMPLES		(512)
#define FFT_SIZE 		(FFT_SAMPLES / 2)

typedef struct Filter_t{
	uint32_t History[WindowLength]; /*Array to store values of filter window*/
	uint32_t Sum;					/* Sum of filter window's elements*/
	uint32_t WindowPointer; 		/* Pointer to the first element of window*/
}Filter_t;



typedef struct results_t{
	uint32_t frequency;
	uint32_t temperature;
}measurement_t;

void Moving_Average_Init(Filter_t* filter_struct)
{
	filter_struct->Sum = 0;
	filter_struct->WindowPointer = 0;

	for(uint32_t i=0; i<WindowLength; i++)
	{
		filter_struct->History[i] = 0;
	}
}

uint32_t Moving_Average_Compute(uint32_t raw_data, Filter_t* filter_struct)
{
	filter_struct->Sum += raw_data;
	filter_struct->Sum -= filter_struct->History[filter_struct->WindowPointer];
	filter_struct->History[filter_struct->WindowPointer] = raw_data;
	if(filter_struct->WindowPointer < WindowLength - 1)
	{
		filter_struct->WindowPointer += 1;
	}
	else
	{
		filter_struct->WindowPointer = 0;
	}
	return filter_struct->Sum/WindowLength;
}

float32_t Input[FFT_SAMPLES];
float32_t Output[FFT_SAMPLES];
float32_t FreqTab[FFT_SIZE];
float32_t FreqOrder[FFT_SIZE];
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for TempFreqReader */
osThreadId_t TempFreqReaderHandle;
const osThreadAttr_t TempFreqReader_attributes = {
  .name = "TempFreqReader",
  .stack_size = 300 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for printer */
osThreadId_t printerHandle;
const osThreadAttr_t printer_attributes = {
  .name = "printer",
  .stack_size = 300 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for QueueTempFreq */
osMessageQueueId_t QueueTempFreqHandle;
const osMessageQueueAttr_t QueueTempFreq_attributes = {
  .name = "QueueTempFreq"
};
/* Definitions for QueuePrint */
osMessageQueueId_t QueuePrintHandle;
const osMessageQueueAttr_t QueuePrint_attributes = {
  .name = "QueuePrint"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void TempFreqTask(void *argument);
void PrintData(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of QueueTempFreq */
  QueueTempFreqHandle = osMessageQueueNew (20, sizeof(Msg), &QueueTempFreq_attributes);

  /* creation of QueuePrint */
  QueuePrintHandle = osMessageQueueNew (20, sizeof(Msg), &QueuePrint_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of TempFreqReader */
  TempFreqReaderHandle = osThreadNew(TempFreqTask, NULL, &TempFreqReader_attributes);

  /* creation of printer */
  printerHandle = osThreadNew(PrintData, NULL, &printer_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_TempFreqTask */
/**
* @brief Function implementing the TempFreqReader thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TempFreqTask */
void TempFreqTask(void *argument)
{
  /* USER CODE BEGIN TempFreqTask */
	// Temp coeffs
	uint16_t TS_CAL1 = *TEMPSENSOR_CAL1_ADDR;
	uint16_t TS_CAL2 = *TEMPSENSOR_CAL2_ADDR;

  // Moving average
	Filter_t avgFilter;
	Moving_Average_Init(&avgFilter);

  // FFT calculations
	uint16_t freqBufferIndex = 0;
	float32_t maxFFTValue = 0;
	uint32_t idxOfMaxFFTValue = 0;
	arm_rfft_fast_instance_f32 FFTHandler;
	for(uint16_t i = 0 ; i < FFT_SIZE ; i++)
	{
		float32_t Fs =  (RCC_MAX_FREQUENCY/(htim8.Init.Prescaler+1))/(htim8.Init.Period+1);
		FreqOrder[i] = i * Fs / FFT_SAMPLES;
	}

  // Messages
	Msg msgFromAdc;
	Msg msgToPrint;
//
//	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
////	HAL_ADC_Start_IT(&hadc1);
//	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adcDMAData, 2);

	arm_rfft_fast_init_f32(&FFTHandler, FFT_SAMPLES);

  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(tempPin_GPIO_Port, tempPin_Pin);
    if (osMessageQueueGetCount(QueueTempFreqHandle) != 0)
    {
      osMessageQueueGet(QueueTempFreqHandle, (void*)&msgFromAdc, 0U, 0U);

      // Temperature calculation
      uint32_t temp = (msgFromAdc.adcV_temp - TS_CAL1) * (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP);
      temp = temp/(TS_CAL2 -TS_CAL1) + TEMPSENSOR_CAL1_TEMP;
      uint32_t avgTemp = Moving_Average_Compute((uint32_t)temp, &avgFilter);

      // Frequency calculation
      Input[freqBufferIndex] = (float32_t)msgFromAdc.adcV_freq;
      freqBufferIndex++;
      if (freqBufferIndex >= FFT_SAMPLES)
      {
        freqBufferIndex=0;
        arm_rfft_fast_f32(&FFTHandler, Input, Output, 0);
        arm_cmplx_mag_f32(Output, FreqTab, FFT_SIZE);
        arm_max_f32(FreqTab+1, FFT_SIZE-1, &maxFFTValue, &idxOfMaxFFTValue);
      }

      // Send to print
      msgToPrint.adcV_temp = avgTemp;
      msgToPrint.adcV_freq = FreqOrder[idxOfMaxFFTValue+1];
      osMessageQueuePut(QueuePrintHandle, (void*)&msgToPrint, 0U, 0U);
    }
	HAL_GPIO_TogglePin(tempPin_GPIO_Port, tempPin_Pin);
    osDelay(1);
  }
  /* USER CODE END TempFreqTask */
}

/* USER CODE BEGIN Header_PrintData */
/**
* @brief Function implementing the printer thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_PrintData */
void PrintData(void *argument)
{
  /* USER CODE BEGIN PrintData */

	Msg ready_msg;

  /* Infinite loop */
	for(;;)
	{
		HAL_GPIO_TogglePin(freqPin_GPIO_Port, freqPin_Pin);
		if (osMessageQueueGetCount(QueuePrintHandle) != 0)
		{
			osMessageQueueGet(QueuePrintHandle, (void*)&ready_msg, 0U, 0U);
			printf("Temp: %d C, freq: %d Hz\r\n", ready_msg.adcV_temp, ready_msg.adcV_freq);
		}
		HAL_GPIO_TogglePin(freqPin_GPIO_Port, freqPin_Pin);
		osDelay(1);
	}
  /* USER CODE END PrintData */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

